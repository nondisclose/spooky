local solution = {}

function solution.odd_or_even(arr)
	local v = 0
	for _,n in pairs(arr) do v = v + n end
	if v % 2 == 0 then return "Even" else return "Odd" end
end



return solution
