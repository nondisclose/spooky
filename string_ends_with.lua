kata = {}

function kata.strEndsWith(s, ending)
	return ending == "" or s:sub(-#ending) == ending
end

return kata
