local solution = {}
function solution.accnum(s)
	local tbl = {}
	local counter = 0
	for c in s:gmatch"." do
		local output = ""
		output = output .. string.upper(c)
		for i = 0,counter-1,1 do
			output = output .. string.lower(c)
		end
		table.insert(tbl, output)
		counter = counter + 1
	end
	return table.concat(tbl, "-")
end
return solution

--accum("abcd") -> "A-Bb-Ccc-Dddd"
--accum("RqaEzty") -> "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
--accum("cwAt") -> "C-Ww-Aaa-Tttt"
