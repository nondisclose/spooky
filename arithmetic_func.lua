local kata = {}

function kata.arithmetic(a, b, operator)
	local val = 0
	if operator == "add" then val = a + b end
	if operator == "subtract" then val = a - b end
	if operator == "multiply" then val = a * b end
	if operator == "divide" then val = a / b end
	return val
end


return kata
